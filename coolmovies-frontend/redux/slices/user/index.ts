export { actions as userActions } from './slice';
export { default as userReducer } from './slice';
import { combineEpics } from 'redux-observable';
import { Login, Register } from './epics';

export const userEpics = combineEpics(Login, Register);
