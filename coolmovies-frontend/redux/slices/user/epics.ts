import { gql } from '@apollo/client';
import { Epic, StateObservable } from 'redux-observable';
import { Observable } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';
import { RootState } from '../../store';
import { EpicDependencies } from '../../../types';
import { actions, SliceAction } from './slice';
import md5 from 'md5';

export const Login: Epic = (
  action$: Observable<SliceAction['login']>,
  state$: StateObservable<RootState>,
  { client }: EpicDependencies
) =>
  action$.pipe(
    filter(actions.login.match),
    switchMap(async (action) => {
      try {
        const result = await client.query({
          query: login,
          variables: {
            name: `${action.payload.name}`,
            password: `${md5(action.payload.password)}`
          }
        });
        if (result.data.allUsers.nodes.length) {
          localStorage.setItem("@ecototal:user", JSON.stringify(result.data.allUsers.nodes[0]))
          // else
          // fire error
          return actions.loadedLogin({ data: result.data });
        }
        return { data: {} }
      } catch (err) {
        return actions.loadError();
      }
    })
  );

export const Register: Epic = (
  action$: Observable<SliceAction['register']>,
  state$: StateObservable<RootState>,
  { client }: EpicDependencies
) =>
  action$.pipe(
    filter(actions.register.match),
    switchMap(async (action) => {
      try {
        const result = await client.query({
          query: login,
          variables: {
            name: `${action.payload.name}`,
            password: `${md5(action.payload.password)}`
          }
        });
        const result2 = await client.mutate({
          mutation: register,
          variables: {
            id: `${md5(action.payload.name)}`,
            name: `${action.payload.name}`,
            password: `${md5(action.payload.password)}`
          }
        });
        localStorage.setItem("@ecototal:user", JSON.stringify({ id: md5(action.payload.name), name: action.payload.name }))
        return actions.loadedRegister({ data: result2.data });
        // else
        // fire error
      } catch (err) {
        return actions.loadError();
      }
    })
  );

const login = gql`
  query allUsers($name: String!, $password: String!) {
    allUsers(filter: {name: {equalTo: $name}, password:{equalTo: $password}}) {
      nodes {
        id
        name
      }
    }
  }
`;

const register = gql`
  mutation MyMutation($id: UUID, $name: String!, $password: String!) {
    createUser(input: {user: {id: $id, name: $name, password: $password}}) {
      user {
      id
      name

      }
    }
  }
`
