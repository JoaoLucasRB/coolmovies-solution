import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Login } from '../../../types';

interface UserState {
  fetchLogin?: unknown[];
}

const initialState: UserState = {};

export const slice = createSlice({
  initialState,
  name: 'user',
  reducers: {
    login: (state, action: PayloadAction<Login>) => {},
    clearDataLogin: (state) => {
      state.fetchLogin = undefined;
    },
    loadedLogin: (state, action: PayloadAction<{ data: unknown[] }>) => {
      state.fetchLogin = action.payload.data;
    },

    register: (state, action: PayloadAction<Login>) => {},
    clearDataRegister: (state) => {
      state.fetchLogin = undefined;
    },
    loadedRegister: (state, action: PayloadAction<{ data: unknown[] }>) => {
      state.fetchLogin = action.payload.data;
    },

    loadError: (state) => {
      state.fetchLogin = ['Error Fetching :('];
    },
  },
});

export const { actions } = slice;
export type SliceAction = typeof actions;
export default slice.reducer;