export { actions as movieActions } from './slice';
export { default as movieReducer } from './slice';
import { combineEpics } from 'redux-observable';
import { movieFetchLast, movieFetchAll, movieFetchOne } from './epics';

export const movieEpics = combineEpics(movieFetchLast, movieFetchAll, movieFetchOne);
