import { gql } from '@apollo/client';
import { Epic, StateObservable } from 'redux-observable';
import { Observable } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';
import { RootState } from '../../store';
import { EpicDependencies } from '../../../types';
import { actions, SliceAction } from './slice';

export const movieFetchLast: Epic = (
  action$: Observable<SliceAction['fetchLast']>,
  state$: StateObservable<RootState>,
  { client }: EpicDependencies
) =>
  action$.pipe(
    filter(actions.fetchLast.match),
    switchMap(async () => {
      try {
        const result = await client.query({
          query: fetchLast,
        });
        return actions.loadedLast({ data: result.data });
      } catch (err) {
        return actions.loadError();
      }
    })
  );

export const movieFetchAll: Epic = (
  action$: Observable<SliceAction['fetchAll']>,
  state$: StateObservable<RootState>,
  { client }: EpicDependencies
) =>
  action$.pipe(
    filter(actions.fetchAll.match),
    switchMap(async () => {
      try {
        const result = await client.query({
          query: fetchAll,
        });
        return actions.loadedAll({ data: result.data });
      } catch (err) {
        return actions.loadError();
      }
    })
  );

export const movieFetchOne: Epic = (
  action$: Observable<SliceAction['fetchOne']>,
  state$: StateObservable<RootState>,
  { client }: EpicDependencies
) =>
  action$.pipe(
    filter(actions.fetchOne.match),
    switchMap(async (action) => {
      try{
        const result = await client.query({
          query: fetchOne,
          variables: {
            id: action.payload
          }
        });
        return actions.loadedOne({ data: result.data })
      } catch (err) {
        return actions.loadError();
      }
    })
  )

const fetchLast = gql`
  query lastMovie {
    allMovies(orderBy: RELEASE_DATE_DESC, first: 1) {
      nodes {
        id
        title
        movieDirectorId
        releaseDate
        imgUrl
        movieReviewsByMovieId(first: 1, orderBy: RATING_DESC) {
          nodes {
            id
            body
            title
            movieId
            userReviewerId
            rating
            userByUserReviewerId {
              id
              name
            }
          }
        }
      }
    }
  }
`;

const fetchAll = gql`
  query allMovies {
    allMovies {
      nodes {
        id
        title
        releaseDate
        imgUrl
        movieDirectorId
        movieDirectorByMovieDirectorId {
          id
          name
          age
        }
        movieReviewsByMovieId {
          nodes {
            rating
          }
        }
      }
    }
  }
`

const fetchOne = gql`
  query movieById($id: UUID!) {
    movieById(id: $id) {
      id
      title
      imgUrl
      releaseDate
      movieDirectorId
      movieDirectorByMovieDirectorId {
        id
        name
      }
      movieReviewsByMovieId {
        nodes {
          id
          title
          body
          rating
          userByUserReviewerId {
            id
            name
          }
          commentsByMovieReviewId {
            nodes {
              id
              title
              body
              userByUserId {
                id
                name
              }
            }
          }
        }
      }
    }
  }
`