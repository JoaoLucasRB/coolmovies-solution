import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AllMovies, LastMovie, MovieById } from '../../../types';

interface MovieState {
  fetchLast?: LastMovie;
  fetchAll?: AllMovies;
  fetchOne?: MovieById;
}

const initialState: MovieState = {
};

export const slice = createSlice({
  initialState,
  name: 'movie',
  reducers: {
    loadError: (state) => {
      state.fetchLast = ['Error Fetching :('];
    },

    fetchLast: () => { },
    clearDataLast: (state) => {
      state.fetchLast = undefined;
    },
    loadedLast: (state, action: PayloadAction<{ data: unknown[] }>) => {
      state.fetchLast = action.payload.data;
    },

    fetchAll: () => { },
    clearDataAll: (state) => {
      state.fetchAll = undefined;
    },
    loadedAll: (state, action: PayloadAction<{ data: unknown[] }>) => {
      state.fetchAll = action.payload.data;
    },

    fetchOne: (state, action: PayloadAction<string>) => { },
    clearDataOne: (state) => {
      state.fetchAll = undefined;
    },
    loadedOne: (state, action: PayloadAction<{ data: unknown[] }>) => {
      state.fetchOne = action.payload.data;
    },
  },
});

export const { actions } = slice;
export type SliceAction = typeof actions;
export default slice.reducer;
