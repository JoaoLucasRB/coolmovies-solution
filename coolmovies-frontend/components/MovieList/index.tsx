import Link from "next/link";
import React, { useEffect } from "react";
import { movieActions, useAppDispatch, useAppSelector } from "../../redux";
import { Container, MovieListItem } from "./style";

export function MovieList() {
  const dispatch = useAppDispatch();
  const movieState = useAppSelector((state) => state.movie);

  useEffect(() => {
      dispatch(movieActions.fetchAll());
  }, []);
  
  return (
    <Container>
      <h2>All Movies</h2>
      <div>
        {movieState.fetchAll?.allMovies?.nodes.map(movie =>
          <Link href={`/reviews/${movie.id}`}>
            <MovieListItem key={`MOVIELIST_HOME_${movie.id}`}>
              <img src={movie.imgUrl} />
              <div>
                <p>{movie.title}</p>
                <span>Release Date: {movie.releaseDate}</span>
                <span>Director: {movie.movieDirectorByMovieDirectorId?.name}</span>
              </div>
            </MovieListItem>
          </Link>
        )}
      </div>
    </Container>
  );
}