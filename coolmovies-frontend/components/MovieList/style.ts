import styled from '@emotion/styled';

export const Container = styled.div`
  height: 100%;
  
  @media(max-width: 1130px) {
    & {
      margin: 0 1rem;
    }
  }

  @media(max-width: 1090px) {
    & {
      margin: 0 2rem;
    }
  }

  & > h2 {
    color: white;
    margin-top: 2rem;
  }

  & > div {
    overflow-y: auto;
    width: 100%;
    height: 100%;
  } 
`;

export const MovieListItem = styled.div`
  display: flex;
  align-items: center;
  height: 9rem;
  margin: 1rem 0;
  transition: background 0.3s ease-in-out;

  &:hover{
    background: #333;
    cursor: pointer;
  }

  & > img {
    background-color: red;
    height: 100%;
    width: 6rem;
    object-fit: cover;
  }

  & > div {
    padding: 0 2rem;
    display: flex;
    flex-direction: column;

    & > p {
      color: white;
      font-size: 2rem;
      font-weight: 700;
      margin: 0;
      margin-bottom: 1rem;
    }

    & > span {
      color: #ccc;
    }
  };
`