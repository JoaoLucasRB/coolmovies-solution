import styled from '@emotion/styled'

export const Container = styled.div`
  margin-top: 2rem;
`

export const MovieContent = styled.div`
  display: flex;
  align-items: center;
  & > img {
    width: 15rem;
    height: 20rem;
    object-fit: cover;
  }

  & > div {
    display: flex;
    flex-direction: column;
    padding-left: 2rem;

    & > p {
      color: white;
      font-weight: 700;
      font-size: 3rem;
      margin-top: 0;
    }

    & > span {
      color: #ccc;
      font-size: 1.3rem;
      padding: 0.5rem 0;
    }
  }

  & > span {
    color: #ccc;
  }
`