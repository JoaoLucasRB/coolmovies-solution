import React from 'react';
import { Movie, MovieReview } from "../../types";
import { Container, MovieContent } from './style';

interface MovieDetailsProps {
  movie: Movie | undefined
}

export function MovieDetails(props: MovieDetailsProps) {

  function getAverageRating() {
    const reviews: MovieReview[] | undefined = props.movie?.movieReviewsByMovieId?.nodes;
    let total = 0, count = 0;
    if (!reviews?.length)
      return 0;
    for (let review of reviews) {
      count++;
      total += review.rating;
    }
    return total / count;
  }

  return (
    props.movie ?
      <Container>
        <MovieContent>
          <img src={props.movie.imgUrl} />
          <div>
            <p>{props.movie.title}</p>
            <span>Release Date: {props.movie.releaseDate}</span>
            <span>Director: {props.movie.movieDirectorByMovieDirectorId?.name}</span>
            <span>Rating: {getAverageRating()}/5</span>
          </div>
        </MovieContent>
      </Container>
      : <></>
  )
}