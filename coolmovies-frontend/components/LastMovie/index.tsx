import React from 'react';
import { Button } from "@mui/material";
import { useEffect } from "react";
import { movieActions, useAppDispatch, useAppSelector } from "../../redux";
import { Author, MovieItem, Container, Review, Title } from "./style";

export function LastMovie() {
  const dispatch = useAppDispatch();
  const movieState = useAppSelector((state) => state.movie);

  useEffect(() => {
      dispatch(movieActions.fetchLast());
  }, []);

  return (
    movieState.fetchLast ?
      <Container>
        <h2>Latest Movie</h2>
        <MovieItem>
          <img src="https://observatoriodocinema.uol.com.br/wp-content/uploads/2020/06/rogue-one-a-star-wars-story.jpeg" />
          <div>
            <Title>{movieState.fetchLast?.allMovies?.nodes[0].title}</Title>
            <Review>" {movieState.fetchLast.allMovies?.nodes[0].movieReviewsByMovieId?.nodes[0].title} "</Review>
            <Author>{movieState.fetchLast.allMovies?.nodes[0].movieReviewsByMovieId?.nodes[0].userByUserReviewerId.name}</Author>
            <Button>See More</Button>
          </div>
        </MovieItem>
      </Container>
      : <></>
  )
}