import styled from '@emotion/styled';

export const Container = styled.div`
  & > h2 {
    color: white;
  }
`

export const MovieItem = styled.div`
width: 100%;
height: 25vh;
  background: '#1f1f1f';
  position: relative;

  &:hover > img {
    mask-image: linear-gradient(to top, rgba(0,0,0,0), rgba(0,0,0,1));
  }
  &:hover > div > button, &:hover > div > span {
    display: block;
  }
  &:hover > div > p {
    text-shadow: none;
  }


  & > img {
    height: 100%;
    width: 100%;
    position: absolute;
    background: #ccc;
    z-index: 1000;
    border: none;
    padding: 0;
    margin: 0;
    border-style: none;
    object-fit: cover;
    transition: mask-image 0.3s ease-in-out;
  }

  & > div {
    position: absolute;
    bottom: 0;
    z-index: 1001;
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 2rem;
    width: 100%;

    & > button {
      background: #730000;
      color: white;
      width: 12rem;
      display: none;

      &:hover {
        background: #610000;
      }
    }
  }
`

export const Title = styled.p`
  color: white;
  width: 100%;
  font-weight: 700;
  font-size: 1.5rem;
  display: block;
  text-shadow: 0px 2px 5px rgba(0, 0, 0, 1);
`

export const Review = styled.span`
  width: 100%;
  font-weight: 600;
  color: #fff;
  display: none;

`
export const Author = styled.span`
  width: 100%;
  font-weight: 400;
  color: #ccc;
  display: none;

  &::before {
    content: '- '
  }
`