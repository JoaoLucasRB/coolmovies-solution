import styled from '@emotion/styled';

export const Root = styled.div`
  min-height: 95vh;
  height: 100%;
  width: 100%;
  margin: 0 auto;
  background: #1f1f1f;
  display: flex;
  justify-content: center;
`

export const Container = styled.div`
  width: 100%;
  max-width: 1080px;
  height: 100%;
  background: #1f1f1f;
`