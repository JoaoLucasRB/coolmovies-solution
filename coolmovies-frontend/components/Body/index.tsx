import { ReactNode } from "react";
import { Container, Root } from "./style";

interface BodyProps {
  children: ReactNode
}

export function Body({children}: BodyProps) {
  return (
    <Root>
      <Container>
        {children}
      </Container>
    </Root>
  )
}