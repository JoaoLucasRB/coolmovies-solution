import { Button, FormControl, InputLabel, OutlinedInput, TextField } from '@mui/material';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { MovieReview } from '../../types';
import { Container, Review } from './style';

interface ReviewsListProps {
  reviews: MovieReview[] | undefined
}

export function ReviewsList(props: ReviewsListProps) {
  const router = useRouter();
  const [newReview, setNewReview] = useState({
    title: '',
    body: '',
    rating: 0,
  });

  const handleChange = (prop: string) => (event: any) => {
    setNewReview({ ...newReview, [prop]: event.target.value });
  };

  useEffect(() => { }, [router.isReady])
  return (
    props.reviews?.length ?
      <Container>
        <h3>Reviews</h3>
        {props.reviews.map(review =>
          <Review>
            <div>
              <p>{review.title}</p>
              <p>Rating: {review.rating}/5</p>
            </div>
            <p>{review.body}</p>
            <span>{review.userByUserReviewerId.name}</span>
          </Review>
        )}

      </Container>
      : <></>
  )
}