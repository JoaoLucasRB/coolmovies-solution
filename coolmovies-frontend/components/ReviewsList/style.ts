import styled from "@emotion/styled";

export const Container = styled.div`
  padding: 2rem 0;

  & > h3 {
    color: white;
  }
`

export const Review = styled.div`
  transition: background 0.3s ease-in-out;
  padding: 0 2rem;
  
  &:hover {
    background: #333;
  }

  & > div {
    display: flex;
    justify-content: space-between;

    & > p {
      color: white;
      font-weight: 700;
      font-size: 1.3rem;
    }
  }

  & > p {
    color: #ccc;
    font-weight: 600;
  }

  & > span {
    color: #ccc;
    font-size: 0.9rem;
    width: 100%;
    display: flex;
    justify-content: flex-end;
  }
`