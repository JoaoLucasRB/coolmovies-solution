import React, { useEffect } from 'react';
import { Button, FormControl, InputLabel, OutlinedInput, Paper, Popover, Typography } from "@mui/material";
import { Container, Content, PopoverContainer } from "./styles";
import Link from "next/link";
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import { useState } from "react";
import { Login, User } from '../../types';
import { useAppDispatch, useAppSelector, userActions } from '../../redux';


export function NavBar() {
  const dispatch = useAppDispatch();
  const userState = useAppSelector((state) => state.user);
  const [user, setUser] = useState<{ id: string, name: string }>({
    id: '',
    name: ''
  });
  const [register, setRegister] = useState<boolean>(false);

  const [anchorEl, setAnchorEl] = useState(null);
  const [loginData, setLoginData] = React.useState<Login>({
    name: '',
    password: '',
  });

  const handlePopover = (event: any) => {
    setAnchorEl(event.currentTarget);
  }

  const handleClose = () => {
    setAnchorEl(null);
  }

  const handleChange = (prop: string) => (event: any) => {
    setLoginData({ ...loginData, [prop]: event.target.value });
  };

  const handleLogin = () => {
    dispatch(userActions.login(loginData));
    console.log(localStorage.getItem('@ecototal:user'));
  }

  const handleLogout = () => {
    dispatch(userActions.clearDataLogin());
    localStorage.removeItem('@ecototal:user');
    setUser({ id: '', name: '' });
    setRegister(false);
  }

  useEffect(() => {
    if (userState.fetchLogin) {
      setUser(JSON.parse(localStorage.getItem('@ecototal:user') || ""));
      console.log(localStorage.getItem('@ecototal:user'));
    }
  }, [userState.fetchLogin]);

  const handleRegister = () => {
    setUser({id: '', name: ''});
    setRegister(true);
  }

  const handleCancelRegister = () => {
    setUser({id: '', name: ''});
    setRegister(false);
  }

  const handleConfirmRegister = () => {
    dispatch(userActions.register(loginData));
    // setUser(JSON.parse(localStorage.getItem('@ecototal:user') || ""));
  }

  const open = Boolean(anchorEl);

  return (
    <>
      <Container>
        <Paper elevation={3}>
          <Content>
            <Link href="/">
              <Typography variant="h1" component="h2">{'EcoPortal'}</Typography>
            </Link>
            {user.id ?
              <span onClick={handlePopover}>Hi, {user.name} <AccountCircleIcon /></span>
              : <span onClick={handlePopover}>Login <AccountCircleIcon /></span>
            }
          </Content>
        </Paper>
      </Container>
      <Popover
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        <PopoverContainer>
          {user.id ?
            <Button onClick={() => handleLogout()}>Logout</Button>
            : !register ?
              <>
                <FormControl sx={{ m: 1, width: '25ch' }} variant="outlined">
                  <InputLabel htmlFor="outlined-adornment-name">Name</InputLabel>
                  <OutlinedInput
                    id="outlined-adornment-name"
                    type='text'
                    value={loginData.name}
                    onChange={handleChange('name')}
                    label="Name"
                  />
                </FormControl>
                <FormControl sx={{ m: 1, width: '25ch' }} variant="outlined">
                  <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
                  <OutlinedInput
                    id="outlined-adornment-password"
                    type='password'
                    value={loginData.password}
                    onChange={handleChange('password')}
                    label="Password"
                  />
                </FormControl>
                <div className='buttons'>
                  <Button className='secondary' onClick={() => handleRegister()}>Register</Button>
                  <Button onClick={() => handleLogin()}>Login</Button>
                </div>
              </>
              : <>
                <FormControl sx={{ m: 1, width: '25ch' }} variant="outlined">
                  <InputLabel htmlFor="outlined-adornment-name">Name</InputLabel>
                  <OutlinedInput
                    id="outlined-adornment-name"
                    type='text'
                    value={loginData.name}
                    onChange={handleChange('name')}
                    label="Name"
                  />
                </FormControl>
                <FormControl sx={{ m: 1, width: '25ch' }} variant="outlined">
                  <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
                  <OutlinedInput
                    id="outlined-adornment-password"
                    type='password'
                    value={loginData.password}
                    onChange={handleChange('password')}
                    label="Password"
                  />
                </FormControl>
                <div className='buttons'>
                  <Button className='secondary' onClick={() => handleCancelRegister()}>Cancel</Button>
                  <Button onClick={() => handleConfirmRegister()}>Register</Button>
                </div>
              </>
          }
        </PopoverContainer>
      </Popover>
    </>
  )
}