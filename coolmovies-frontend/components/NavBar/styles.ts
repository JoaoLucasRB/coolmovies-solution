import styled from '@emotion/styled';

export const Container = styled.div`
  & > div {
    background: #730000;
    height: 5vh;
    align-self: stretch;
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 0;

    p {
      color: white;
    }
  }
`

export const Content = styled.div`
  max-width: 1080px;
  width: 100%;
  display: flex;
  justify-content: space-between;

  @media(max-width: 1130px) {
    & {
      margin: 0 1rem;
    }
  }

  @media(max-width: 1090px) {
    & {
      margin: 0 2rem;
    }
  }

  & > h2 {
    color: white;
    font-weight: 500;
    font-size: 2rem;
    cursor: pointer;
  }

  & > span {
    color: white;
    display: flex;
    align-items: center;
    cursor: pointer;
    transition: color 0.3s ease-in-out;
    font-weight: 700;

    & > svg {
      margin-left: 0.5rem;
    }

    &:hover {
      color: #ddd;
    }
  }
`

export const PopoverContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0;
  background: #1f1f1f;
  border: #ccc !important;
  width: 18rem;
    padding: 0.5rem 1rem;

    & > button {
      border: 1px solid #730000;
      background: #1f1f1f;
      color: white;
      padding: 0.5rem;
      transition: background 0.3s ease-in-out;

      &:hover {
        background: #333;
      }
    }

  & > div {
    border: #ccc !important;
    width: 100%;
    margin: 1rem 0 0.5rem 0 ;
  }
  
  .MuiOutlinedInput-root {
    &.Mui-focused fieldset {
      border-color: #ccc;
    }
  }

  label {
    color: #ccc !important;

    &.Mui-focused {
      /* color: #730000; */
    }
  }
  input {
    color: white;
  }

  div.buttons {
    display: flex;
    justify-content: space-between;
    button {
      border: 1px solid #730000;
      background: #730000;
      color: white;
      padding: 0.5rem;
      transition: background 0.3s ease-in-out;

      &:hover {
        background: #610000;
      }

      &.secondary {
        background: #1f1f1f;

        &:hover {
          background: #333;
        }
      }
    }
  }
`