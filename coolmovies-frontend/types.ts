import { ApolloClient, NormalizedCacheObject } from '@apollo/client';

export type CreateStoreOptions = {
  epicDependencies?: EpicDependencies;
};

export type EpicDependencies = {
  client: ApolloClient<NormalizedCacheObject>;
};

export interface Movie {
  id: string;
  movieDirectorId: string;
  releaseDate: string;
  title: string;
  imgUrl: string;
  movieReviewsByMovieId?: {
    nodes: MovieReview[];
  }
  movieDirectorByMovieDirectorId?: MovieDirector;
  [key: string]: any;
}

export interface MovieDirector {
  id: string;
  name: string;
  age: number;
}

export interface MovieReview {
  id: string;
  title: string;
  body: string;
  rating: number;
  movieId: string;
  userReviewerId: string;
  userByUserReviewerId: User;
}

export interface User {
  id: string;
  name: string;
  password: string;
}

export interface CommentReview {
  id: string;
  title: string;
  body: string;
  movieReviewId: string;
  userId: string;
}

export interface LastMovie {
  allMovies?: {
    nodes: Movie[]
  };
  [key: string]: any
}

export interface AllMovies { 
  allMovies?: {
    nodes: Movie[]
  };
  [key: string]: any
}

export interface MovieById {
  movieById?: Movie;
  [key: string]: any;
}

export interface Login {
  id?: string;
  name: string;
  password: string;
}