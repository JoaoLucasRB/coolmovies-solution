import React, { useEffect, useState } from 'react';
import type { NextPage } from 'next';
import { useRouter } from 'next/router';
import { movieActions, useAppDispatch, useAppSelector } from '../../redux';
import { MovieDetails } from '../../components/MovieDetails';
import { ReviewsList } from '../../components/ReviewsList';


const Reviews: NextPage = () => {
  const router = useRouter();

  const dispatch = useAppDispatch();
  const movieState = useAppSelector((state) => state.movie);

  useEffect(() => {
    if (router.isReady) {
      dispatch(movieActions.fetchOne(`${router.query.movieId}`));
    }
  }, [router.isReady])

  return (
    <>
      <MovieDetails movie={movieState.fetchOne?.movieById} />
      <ReviewsList reviews={movieState.fetchOne?.movieById?.movieReviewsByMovieId?.nodes}/>
    </>
  );
};

export default Reviews;
