import React from 'react';
import { css } from '@emotion/react';
import type { NextPage } from 'next';
import { NavBar } from '../components/NavBar';
import { LastMovie } from '../components/LastMovie';
import { MovieList } from '../components/MovieList';


const Home: NextPage = () => {
  return (
    <>
      <LastMovie />
      <MovieList />
    </>
  );
};

const styles = {
  root: css({
    minHeight: '95vh',
    height: '100%',
    width: '100%',
    margin: '0 auto',
    background: '#1f1f1f',
    display: 'flex',
    justifyContent: 'center',
  }),
  body: css({
    width: '100%',
    maxWidth: '1080px',
    // minHeight: '95vh',
    height: '100%',
    background: '#1f1f1f',
  }),
  heading: css({ marginTop: 16, fontSize: '2.75rem', textAlign: 'center' }),
  subtitle: css({
    fontWeight: 300,
    textAlign: 'center',
    maxWidth: 600,
    margin: '24px 0',
    color: 'rgba(0, 0, 0, 0.6)',
  }),
  mainControls: css({
    display: 'flex',
    alignItems: 'center',
    button: { marginRight: 16 },
  }),
  dataInput: css({
    alignSelf: 'stretch',
    margin: '32px 0',
  }),
};

export default Home;
